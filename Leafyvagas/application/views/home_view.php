
<div class="bannerindex">
    <div class="bannerindexcontent">
        <img style="width: 40%;" src="<?=base_url('/assets/img/logobranco.png')?>">
        <br><br>
        <p style="color:#fff;">Anunciando uma vaga ou oferecendo um serviço?</p>
        <br>
        <a href="login.html" class="normalbutton">Comece agora!</a>
    </div>
    </div>
    <div class="objetivos">
    <div class="objetivotext">
        <h1>Nossos Objetivos</h1>
        <p>Analisar as tecnologias existentes de busca por empregos e divulgação, conhecer as necessidades específicas e criar uma plataforma que facilite a busca por emprego e serviços autônomos na região. 
            <br><br>Facilitando tanto para quem está buscando um emprego formal, divulgando ou procurando um serviço, diminuindo a necessidade de pesquisa em diversos sites e realização de pagamento para utilizar as plataformas.</p>
    </div>
    <div>
        <img src="<?=base_url('/assets/img/indeximg1.png')?>"></div>
    </div>
</div>