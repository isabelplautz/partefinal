<!DOCTYPE html>
<html>
    <head>
    <title><?= $title ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?=base_url('/assets/css/mainstyle.css')?>" rel="stylesheet" type="text/css"/>
        <link href="<?=base_url('/assets/css/headerfooter.css')?>" rel="stylesheet" type="text/css"/>
        <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <header>
            <div id="mainheader">
                <div id="headertop">
                    <a href="<?= base_url('')?>adicionaranun" class="normalbutton">Adicionar anúncio</a>
                    <a class="mainlogolink" href="<?= base_url('')?>home"><img class="mainlogo" src="<?=base_url('/assets/img/logo.png')?>"></a>
                    <a href="<?= base_url('')?>login" class="normalbutton">Entrar</a>
                </div>
                <div id="headerbottom">
                    <a href="<?= base_url('')?>vagas" class="specialbutton">Vagas</a>
                    <a href="<?= base_url('')?>ofertas" class="specialbutton">Ofertas</a>
                </div>
            </div>
        </header>