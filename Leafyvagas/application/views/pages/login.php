<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<div class="logincad">
    <div class="mainbox">
        <h1>Fazer Login</h1>
        <br>
        <hr class="solid">
        <br>
        <div id="allloginform" class="loginformdiv">
            <div class="headerlogin"></div>
            <div class="loginform">
                <form action="#" method="get">
                    <input type="email" name="email" placeholder="E-mail"><br><br>
                    <input type="password" name="senha" id="senha" placeholder="Senha" />
                    <br><br>
                    <div class="bottomformlogin">
                        <div style="display: flex; align-items: center;">
                            <input style="width: fit-content;" type="checkbox" name="lembrarme" value="Lembrar-me">
                            <label for="lembrarme">Lembrar-me</label><br>
                        </div>
                        <div>
                            <a class="link" href="#">Esqueci minha senha</a>
                        </div>
                    </div>
                    
                    <input class="normalbuttonfull" type="submit" value="Entrar">
                </form>
            </div>
        </div>
    </div>
    <div class="mainbox">
        <h1>Fazer Cadastro</h1>
        <br>
        <hr class="solid">
        <br>
        <div style="display: grid;">
            <a href="<?= base_url('')?>cadastropfisica" class="normalbuttonfull">PESSOA FÍSICA</a>
            <br><br>
            <a href="<?= base_url('')?>cadastropjuridica" class="normalbuttonfull">PESSOA JURÍDICA</a>
        </div>
        
    </div>
</div>