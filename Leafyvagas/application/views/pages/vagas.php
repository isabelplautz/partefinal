<link href="<?=base_url('/assets/css/anuncios.css')?>" rel="stylesheet" type="text/css"/>

<body>
    <div class="anuncios">
        <h2>0 vaga em Sapiranga</h2>
        <br>
        <div class="filtros">
            <h2>Filtros</h2>
            <br>
            <p>Cidades:</p>
            <form action="" method="post">
                <input type="checkbox" name="ararica" value="Araricá">
                <label for="ararica">Araricá</label><br>
                <input type="checkbox" name="campobom" value="Campo Bom">
                <label for="campobom">Campo Bom</label><br>
                <input type="checkbox" name="doisirmaos" value="Dois Irmãos">
                <label for="doisirmaos">Dois Irmãos</label><br>
                <input type="checkbox" name="novahartz" value="Nova Hartz">
                <label for="novahartz">Nova Hartz</label><br>
                <input type="checkbox" name="novohamburgo" value="Novo Hamburgo">
                <label for="novohamburgo">Novo Hamburgo</label><br>
                <input type="checkbox" name="sapiranga" value="Sapiranga" checked>
                <label for="sapiranga">Sapiranga</label><br>
                <br>
                </form>
                <br>
                <p>Média salarial:</p>
                <div class="wrapper">
                <div class="values">
                    <span id="range1">0</span>
                    <span> &dash; </span>
                    <span id="range2">100</span>
                </div>
                <div class="container">
                    <div class="slider-track"></div>
                    <input type="range" min="0" max="100" value="30" id="slider-1" oninput="slideOne()">
                    <input type="range" min="0" max="100" value="70" id="slider-2" oninput="slideTwo()">
                </div>
            </div>
            <p>Realização do Trabalho:</p>
            <form action="" method="post">
                <input type="checkbox" name="domicilio" value="A domicílio">
                <label for="domicilio">A domicílio</label><br>
                <input type="checkbox" name="remoto" value="Remoto">
                <label for="remoto">Remoto</label><br>
                <input type="checkbox" name="acombinar" value="Local a combinar">
                <label for="acombinar">Local a combinar</label><br>
                <br>
                </form>
                <br>
                <p>Tipo de Oferta:</p>
            <form action="" method="post">
                <input type="checkbox" name="freelancer" value="Freelancer">
                <label for="freelancer">Freelancer</label><br>
                <input type="checkbox" name="autonomo" value="Autônomo">
                <label for="autonomo">Autônomo</label><br>
                <input type="checkbox" name="procurandoemprego" value="Procurando emprego">
                <label for="procurandoemprego">Procurando emprego</label><br>
                <input type="checkbox" name="microempreendedor" value="Microempreendedor">
                <label for="microempreendedor">Microempreendedor</label><br>
                <br>
                </form>
        </div>
        <div class="results">
            <div>

            </div>
        </div>
    </div>
    <script src="<?=base_url('/assets/js/rangeslider.js')?>"></script>
</body>   