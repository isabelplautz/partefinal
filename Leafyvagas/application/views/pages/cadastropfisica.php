<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<?=base_url('/assets/js/jsendereco.js')?>"></script>
<script src="<?=base_url('/assets/js/validacpf.js')?>"></script>
  <script>
    $(function(){
      $("#enviar").click(function(){
        var senha = $("#senha").val();
        var senha2 = $("#confsenha").val();
        if(senha != senha2){
          event.preventDefault();
            alert("As senhas não são iguais!");
        }
      });
    });
</script>
<script type="text/javascript">
    function fMasc(objeto,mascara) {
        obj=objeto
        masc=mascara
        setTimeout("fMascEx()",1)
    }
    function fMascEx() {
        obj.value=masc(obj.value)

    }
    function mCPF(cpf){
        cpf=cpf.replace(/\D/g,"")
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
        return cpf
    }
    function mCEP(cep){
            cep=cep.replace(/\D/g,"")
            cep=cep.replace(/^(\d{5})(\d)/,"$1-$2")
            return cep
    }
    function mTel(tel) {
            tel=tel.replace(/\D/g,"")
            tel=tel.replace(/^(\d)/,"($1")
            tel=tel.replace(/(.{3})(\d)/,"$1)$2")
            if(tel.length == 9) {
                tel=tel.replace(/(.{1})$/,"-$1")
            } else if (tel.length == 10) {
                tel=tel.replace(/(.{2})$/,"-$1")
            } else if (tel.length == 11) {
                tel=tel.replace(/(.{3})$/,"-$1")
            } else if (tel.length == 12) {
                tel=tel.replace(/(.{4})$/,"-$1")
            } else if (tel.length > 12) {
                tel=tel.replace(/(.{4})$/,"-$1")
            }
            return tel;
        }
</script>
<body>
    <div class="mainbox">
        <div class="cadfisicbox">
            <h1>Pessoa Física</h1>
            <br>
            <hr class="solid">
            <br>
            <form id="cadpfisica" action="<?= base_url() ?>cadastropfisica/cadastroefetuado" method="post">
                <input type="text" id="nome" name="nome" placeholder="Nome Completo*" required><br><br>
                <input type="text" id="cpf" name="cpf" placeholder="CPF*" onblur="validarCPF(this.value);" maxlength="14" onkeydown="javascript: fMasc( this, mCPF );" required><br><br>
                <input type="email" id="email" name="email" placeholder="Email*" required><br><br>
                <div class="formmetade">
                    <div><input type="date" id="datanasc" name="datanasc" placeholder="Data de Nascimento*" required></div>
                    <div><input type="tel" id="numcel" name="numcel" placeholder="Número de celular*" maxlength="14" onkeydown="javascript: fMasc( this, mTel );" required></div>
                </div>
                <br>
                <select id="escolaridade" name="escolaridade" form="cadpfisica">
                    <option selected>Escolaridade</option>
                    <option value="Fundamental - Incompleto">Fundamental - Incompleto</option>
                    <option value="Fundamental - Completo">Fundamental - Completo</option>
                    <option value="Médio - Incompleto">Médio - Incompleto</option>
                    <option value="Médio - Completo">Médio - Completo</option>
                    <option value="Superior - Incompleto">Superior - Incompleto</option>
                    <option value="Superior - Completo">Superior - Completo</option>
                    <option value="Pós-graduação - Incompleto">Pós-graduação - Incompleto</option>
                    <option value="Pós-graduação - Completo">Pós-graduação - Completo</option>
                    <option value="Pós-graduação mestrado - Incompleto">Pós-graduação mestrado - Incompleto</option>
                    <option value="Pós-graduação mestrado - Completo">Pós-graduação mestrado - Completo</option>
                    <option value="Pós-graduação doutorado - Incompleto">Pós-graduação doutorado - Incompleto</option>
                    <option value="Pós-graduação doutorado - Completo">Pós-graduação doutorado - Completo</option>
                    </select>
                    <br><br>
                <input type="password" id="senha" name="senha" placeholder="Senha*" required><br><br>
                <input type="password" id="confsenha" placeholder="Confirmar Senha*" required><br><br>
                <p style="color: #727272;">Endereço</p>
                <input name="cep" type="text" id="cep" value="" size="10" maxlength="9" placeholder="CEP*" onblur="pesquisacep(this.value);" onkeydown="javascript: fMasc( this, mCEP );" required><br><br>
                <div class="formuni">
                    <div style="width: 38%;"><input name="cidade" type="text" id="cidade" size="40" placeholder="Cidade" required><br><br></div>
                    <div style="width: 18%;"><input name="uf" type="text" id="uf" size="2" placeholder="UF" required><br><br></div>
                    <div style="width: 38%;"><input name="bairro" type="text" id="bairro" size="40" placeholder="Bairro" required><br><br></div>
                </div>
                <div class="formuni">
                    <div style="width: 78%;"><input name="rua" type="text" id="rua" size="60" placeholder="Rua" required></div>
                    <div style="width: 18%;"><input name="numerocasa" type="int" id="numerocasa" size="2" placeholder="Número*" required></div>
                </div>
                <p style="color:#B6B6B6;">* Itens obrigatórios</p><br>
                <input class="sendbutton" type="submit" id="enviar" value="CADASTRAR">
                </form>
        </div>
    </div>
    
</body>