
<link href="<?=base_url('/assets/css/formanuncio.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<body>
  <div class="mytabs">
    <input type="radio" id="tabvagas" name="mytabs" checked="checked">
    <label for="tabvagas">Vagas</label>
    <div class="tab">
        <form id="anunciovaga" action="<?= base_url() ?>cadastropfisica/cadastroefetuado" method="post">
            <input type="text" id="titulo" name="titulo" placeholder="Título da Vaga*" required><br><br>
            <input type="text" id="local" name="local" placeholder="Local do trabalho*" required><br><br>
            <br>
            <select id="tipodevaga" name="tipodevaga" form="anunciovaga">
                <option selected>Tipo de vaga</option>
                <option value="Meio período">Meio período</option>
                <option value="Estágio">Estágio</option>
                <option value="Integral">Integral</option>
                <option value="CLT">CLT</option>
                <option value="Freelance">Freelance</option>
                </select>
                <br><br>
                <input type="text" id="jornada" name="jornada" placeholder="Jornada de Trabalho (EX: 40h semanais)"><br><br>
                <select id="escolaridade" name="escolaridade" form="cadpfisica">
                    <option selected>Escolaridade</option>
                    <option value="Fundamental - Incompleto">Fundamental - Incompleto</option>
                    <option value="Fundamental - Completo">Fundamental - Completo</option>
                    <option value="Médio - Incompleto">Médio - Incompleto</option>
                    <option value="Médio - Completo">Médio - Completo</option>
                    <option value="Superior - Incompleto">Superior - Incompleto</option>
                    <option value="Superior - Completo">Superior - Completo</option>
                    <option value="Pós-graduação - Incompleto">Pós-graduação - Incompleto</option>
                    <option value="Pós-graduação - Completo">Pós-graduação - Completo</option>
                    <option value="Pós-graduação mestrado - Incompleto">Pós-graduação mestrado - Incompleto</option>
                    <option value="Pós-graduação mestrado - Completo">Pós-graduação mestrado - Completo</option>
                    <option value="Pós-graduação doutorado - Incompleto">Pós-graduação doutorado - Incompleto</option>
                    <option value="Pós-graduação doutorado - Completo">Pós-graduação doutorado - Completo</option>
                    </select>
                    <br><br>
                    <textarea rows="5" id="descvaga" name="descvaga" placeholder="Descrição da vaga*" required></textarea>
                    <input type="text" id="formacontato" name="formacontato" placeholder="Forma de contato*" required><br><br>
            <p style="color:#B6B6B6;">* Itens obrigatórios</p><br>
            <input class="sendbutton" type="submit" id="enviar" value="CADASTRAR">
            </form>
    </div>

    <input type="radio" id="tabofertas" name="mytabs">
    <label for="tabofertas">Ofertas</label>
    <div class="tab">
    <form id="anunciovaga" action="<?= base_url() ?>cadastropfisica/cadastroefetuado" method="post">
            <input type="text" id="titulo" name="titulo" placeholder="Título da Oferta*" required><br><br>
            <input type="text" id="local" name="local" placeholder="Local de atuação*" required><br><br>
            <br>
            <select id="tipodevaga" name="tipodevaga" form="anunciovaga">
                <option selected>Tipo de Oferta</option>
                <option value="Freelancer">Freelancer</option>
                <option value="Autônomo">Autônomo</option>
                <option value="Procurando Emprego">Procurando Emprego</option>
                <option value="Microempreendedor">Microempreendedor</option>
                </select>
                <br><br>
                    <textarea rows="5" id="descoferta" name="descoferta" placeholder="Descrição da oferta*" required></textarea>
                    <input type="text" id="formacontato" name="formacontato" placeholder="Forma de contato*" required><br><br>
            <p style="color:#B6B6B6;">* Itens obrigatórios</p><br>
            <input class="sendbutton" type="submit" id="enviar" value="CADASTRAR">
            </form>
    </div>

  </div>
</body>