<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastropfisica extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
		$data['title'] = "Leafy Vagas - Cadastro de pessoa física";

		$this->load->view('templates/header', $data);
		$this->load->view('pages/cadastropfisica', $data);
		$this->load->view('templates/footer', $data);
    }
    public function cadastroefetuado(){
      $user = $_POST;
      $this->load->model("cadastropfisica_model");
      $this->cadastropfisica_model->cadastroefetuado($user);
      redirect("home");
    }

}