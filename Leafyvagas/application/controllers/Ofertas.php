<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ofertas extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
		$data['title'] = "Leafy Vagas - Ofertas";

		$this->load->view('templates/header', $data);
		$this->load->view('pages/ofertas', $data);
		$this->load->view('templates/footer', $data);
    }

}