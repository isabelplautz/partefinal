<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastropjuridica extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
		$data['title'] = "Leafy Vagas - Cadastro de pessoa jurídica";

		$this->load->view('templates/header', $data);
		$this->load->view('pages/cadastropjuridica', $data);
		$this->load->view('templates/footer', $data);
    }
    public function cadastroefetuado(){
      $user = $_POST;
      $this->load->model("cadastropjuridica_model");
      $this->cadastropjuridica_model->cadastroefetuado($user);
      redirect("home");
    }

}