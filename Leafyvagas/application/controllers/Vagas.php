<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vagas extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
      $data['title'] = "Leafy Vagas - Vagas";

      $this->load->view('templates/header', $data);
      $this->load->view('pages/vagas', $data);
      $this->load->view('templates/footer', $data);
    }

}