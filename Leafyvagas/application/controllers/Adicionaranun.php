<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adicionaranun extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
		$data['title'] = "Leafy Vagas - Adicionar anúncio";

		$this->load->view('templates/header', $data);
		$this->load->view('pages/adicionaranun', $data);
		$this->load->view('templates/footer', $data);
    }

}